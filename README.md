# Fossils 3D Frontend

## Development

Run `npm i` to install all dependencies.

Run `npm run start` to start the development server at `localhost:4200`.

There is a development proxy configured in `proxy.conf.json` to route requests under `/api` to the production backend, but it may be changed to route requests to a development backend instead.

## Deployment

Build with `docker build -t fossils3d-angular-spa .` and deploy with `docker run --name fossils3d-angular-spa --restart unless-stopped --network web fossils3d-angular-spa`

The provided Dockerfile builds a production version of the app and copies the result to an NGINX container. The container exposes the app on port 80, and supports the `http1(.1)` and `h2c` protocols.

A network called `web` is assumed to exist.
