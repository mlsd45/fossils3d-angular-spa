import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {AppModule} from './app.module';
import {RouterTestingModule} from '@angular/router/testing';

describe('AppComponent', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        AppModule,
        RouterTestingModule
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    return expect(app).toBeTruthy();
  });

  it(`should have as title 'fossils3d-angular-spa'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    return expect(app.title).toEqual('fossils3d-angular-spa');
  });

  it('should render tabs in a nav mat-tab-nav-bar', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    return expect(compiled.querySelector('nav')?.getAttribute('mat-tab-nav-bar')).not.toBeNull();
  });
});
