import {Component, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {JsonApiQueryData} from 'angular2-jsonapi';
import {Fossilv2} from '../../api/fossils/fossilv2';
import {distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {ApiService} from '../../api/api.service';
import isEqual from 'lodash-es/isEqual';

@Component({
  selector: 'app-fossils',
  templateUrl: './fossils.component.html',
  styleUrls: ['./fossils.component.css']
})
export class FossilsComponent implements OnInit {

  public page$ = new BehaviorSubject<PageEvent>({pageIndex: 0, pageSize: 10, length: 0});
  public renderMode: 'fast' | 'fancy' = 'fast';
  private queryData$ = (() => {
    const s = new Subject<JsonApiQueryData<Fossilv2>>();
    this.page$.pipe(
      distinctUntilChanged(isEqual),
      switchMap(page => {
        return this.apiService.findAll(Fossilv2, {
          'page[size]': page.pageSize,
          'page[number]': page.pageIndex + 1
        });
      })
    ).subscribe(s);
    return s;
  })();
  public fossils$: Observable<Fossilv2[]> = this.queryData$.pipe(
    map(data => data.getModels())
  );
  public totalItems$ = this.queryData$.pipe(
    map(data => data.getMeta().meta.totalResourceCount as number)
  );

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
  }

  handleCheckboxChange($event: MatCheckboxChange) {
    if ($event.checked) {
      this.renderMode = 'fancy';
    } else {
      this.renderMode = 'fast';
    }
  }
}
