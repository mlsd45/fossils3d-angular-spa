import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {FossilsComponent} from './fossils.component';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {sampleData} from '../../api/api.service.spec';
import {AppModule} from '../../app.module';

describe('FossilsComponent', () => {
  let component: FossilsComponent;
  let fixture: ComponentFixture<FossilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FossilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', inject([HttpTestingController], (httpMock: HttpTestingController) => {
    expect(component).toBeTruthy();
    const request = httpMock.expectOne(req => req.method === 'GET' && req.url.startsWith('./api/v2/fossil'));
    request.flush(sampleData);
  }));

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));
});
