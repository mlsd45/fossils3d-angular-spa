import {Component, Input, OnInit} from '@angular/core';
import {Fossilv2} from '../../../../api/fossils/fossilv2';

@Component({
  selector: 'app-fossil-item',
  templateUrl: './fossil-item.component.html',
  styleUrls: ['./fossil-item.component.css']
})
export class FossilItemComponent implements OnInit {

  @Input()
  public fossil: Fossilv2;

  @Input()
  public renderMode: 'fast' | 'fancy';

  constructor() {
  }

  ngOnInit() {
  }

}
