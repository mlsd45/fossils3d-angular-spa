import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FossilModelComponent} from './fossil-model.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AppModule} from '../../../../../app.module';

describe('FossilModelComponent', () => {
  let component: FossilModelComponent;
  let fixture: ComponentFixture<FossilModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FossilModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
