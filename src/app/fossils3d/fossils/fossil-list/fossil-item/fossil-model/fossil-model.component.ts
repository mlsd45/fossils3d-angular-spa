import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {HttpClient} from '@angular/common/http';
import {ReplaySubject} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

interface SketchfabApi {
  embedUrl: string;
  thumbnails: {
    images: {
      size: number;
      url: string;
    }[];
  };
}

@Component({
  selector: 'app-fossil-model',
  templateUrl: './fossil-model.component.html',
  styleUrls: ['./fossil-model.component.css']
})
export class FossilModelComponent implements OnChanges {

  @Input()
  public modelId: string;

  @Input()
  public renderMode: 'fast' | 'fancy' = 'fast';

  private modelId$ = new ReplaySubject<string>(1);
  private modelData$ = this.modelId$.pipe(
    switchMap(id => {
      return this.http.get<SketchfabApi>(`https://api.sketchfab.com/v3/models/${id}`);
    })
  );
  public modelEmbed$ = this.modelData$.pipe(
    map(data => this.sanitizer.bypassSecurityTrustResourceUrl(data.embedUrl))
  );
  public modelThumbnail$ = this.modelData$.pipe(
    map(data => {
      const largestImage = data.thumbnails.images.reduce(
        (previousValue, currentValue) => previousValue.size > currentValue.size ? previousValue : currentValue);
      return this.sanitizer.bypassSecurityTrustResourceUrl(largestImage.url);
    })
  );

  constructor(private sanitizer: DomSanitizer, private http: HttpClient) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.modelId) {
      this.modelId$.next(changes.modelId.currentValue);
    }
  }
}
