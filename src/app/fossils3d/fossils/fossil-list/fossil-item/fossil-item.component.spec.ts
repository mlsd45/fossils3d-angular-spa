import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FossilItemComponent} from './fossil-item.component';
import {AppModule} from '../../../../app.module';
import {Fossilv2} from '../../../../api/fossils/fossilv2';
import {RouterTestingModule} from '@angular/router/testing';

describe('FossilItemComponent', () => {
  let component: FossilItemComponent;
  let fixture: ComponentFixture<FossilItemComponent>;
  let title: HTMLHeadingElement;
  let fossil: Fossilv2;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fossil = new Fossilv2(undefined, {});
    fossil.name = 'Test';

    fixture = TestBed.createComponent(FossilItemComponent);
    component = fixture.componentInstance;
    component.fossil = fossil;

    title = fixture.nativeElement.querySelector('h1');

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be named', () => {
    expect(title.textContent).toContain(fossil.name);
  });
});
