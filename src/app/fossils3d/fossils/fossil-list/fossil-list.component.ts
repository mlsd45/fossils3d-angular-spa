import {Component, Input} from '@angular/core';
import {Fossilv2} from '../../../api/fossils/fossilv2';

@Component({
  selector: 'app-fossil-list',
  templateUrl: './fossil-list.component.html',
  styleUrls: ['./fossil-list.component.css']
})
export class FossilListComponent {

  @Input()
  public fossils: Fossilv2[];

  @Input()
  public renderMode: 'fast' | 'fancy';

  constructor() {
  }

}
