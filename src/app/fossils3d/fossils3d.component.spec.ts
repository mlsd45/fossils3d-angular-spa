import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {Fossils3dComponent} from './fossils3d.component';
import {AppModule} from '../app.module';

describe('Fossils3dComponent', () => {
  let component: Fossils3dComponent;
  let fixture: ComponentFixture<Fossils3dComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Fossils3dComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
