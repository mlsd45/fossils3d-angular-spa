import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Fossils3dRoutingModule} from './fossils3d-routing.module';
import {Fossils3dComponent} from './fossils3d.component';
import {FossilItemComponent} from './fossils/fossil-list/fossil-item/fossil-item.component';
import {FossilModelComponent} from './fossils/fossil-list/fossil-item/fossil-model/fossil-model.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {JsonApiModule} from 'angular2-jsonapi';
import {FossilsComponent} from './fossils/fossils.component';
import {FossilDetailsComponent} from './details/fossil-details/fossil-details.component';
import {TaxaTreeComponent} from './details/fossil-details/taxa-tree/taxa-tree.component';
import {TaxonDetailsComponent} from './details/taxon-details/taxon-details.component';
import {FossilListComponent} from './fossils/fossil-list/fossil-list.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatChipsModule} from '@angular/material/chips';


@NgModule({
  declarations: [
    Fossils3dComponent,
    FossilItemComponent,
    FossilModelComponent,
    FossilsComponent,
    FossilDetailsComponent,
    TaxaTreeComponent,
    TaxonDetailsComponent,
    FossilListComponent
  ],
  imports: [
    CommonModule,
    Fossils3dRoutingModule,
    FlexLayoutModule,
    JsonApiModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatCardModule,
    MatChipsModule,
    MatButtonModule
  ]
})
export class Fossils3dModule {
}
