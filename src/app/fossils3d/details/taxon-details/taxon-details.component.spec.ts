import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TaxonDetailsComponent} from './taxon-details.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AppModule} from '../../../app.module';

describe('TaxonDetailsComponent', () => {
  let component: TaxonDetailsComponent;
  let fixture: ComponentFixture<TaxonDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxonDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
