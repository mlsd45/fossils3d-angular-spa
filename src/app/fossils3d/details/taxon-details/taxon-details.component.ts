import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../../../api/api.service';
import {map, switchMap} from 'rxjs/operators';
import {Taxonv2} from '../../../api/taxa/taxonv2';

@Component({
  selector: 'app-taxon-details',
  templateUrl: './taxon-details.component.html',
  styleUrls: ['./taxon-details.component.css']
})
export class TaxonDetailsComponent {

  public taxon = this.route.paramMap.pipe(
    map(it => it.get('taxonId')),
    switchMap(it => this.apiService.findRecord(Taxonv2, it, {
      include: 'members,rank'
    }))
  );

  constructor(private route: ActivatedRoute, private apiService: ApiService) {
  }
}
