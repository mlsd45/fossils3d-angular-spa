import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FossilDetailsComponent} from './fossil-details.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AppModule} from '../../../app.module';

describe('FossilDetailsComponent', () => {
  let component: FossilDetailsComponent;
  let fixture: ComponentFixture<FossilDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FossilDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
