import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {ApiService} from '../../../api/api.service';
import {Fossilv2} from '../../../api/fossils/fossilv2';

@Component({
  selector: 'app-fossils3d-details',
  templateUrl: './fossil-details.component.html',
  styleUrls: ['./fossil-details.component.css']
})
export class FossilDetailsComponent {

  public fossil = this.route.paramMap.pipe(
    map(it => it.get('fossilId')),
    switchMap(it => this.apiService.findRecord(Fossilv2, it, {
      include: 'taxa.rank',
      'sort[taxon]': '-rank.ordering'
    }))
  );

  constructor(private route: ActivatedRoute, private apiService: ApiService) {
  }

}
