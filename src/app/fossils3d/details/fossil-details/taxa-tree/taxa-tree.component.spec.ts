import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TaxaTreeComponent} from './taxa-tree.component';

describe('TaxaTreeComponent', () => {
  let component: TaxaTreeComponent;
  let fixture: ComponentFixture<TaxaTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxaTreeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
