import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Fossils3dComponent} from './fossils3d.component';
import {FossilsComponent} from './fossils/fossils.component';
import {FossilDetailsComponent} from './details/fossil-details/fossil-details.component';
import {TaxonDetailsComponent} from './details/taxon-details/taxon-details.component';

const routes: Routes = [
  {
    path: 'fossils3d',
    component: Fossils3dComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: FossilsComponent
      },
      {
        path: 'fossil/:fossilId',
        component: FossilDetailsComponent,
        data: {navigationMode: 'up'}
      },
      {
        path: 'taxon/:taxonId',
        component: TaxonDetailsComponent,
        data: {navigationMode: 'up'}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Fossils3dRoutingModule {
}
