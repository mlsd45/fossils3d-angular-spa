import {Component, ViewChild} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {filter, map} from 'rxjs/operators';
import {MatSidenav} from '@angular/material/sidenav';
import {ActivationStart, Router} from '@angular/router';
import {Location} from '@angular/common';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fossils3d-angular-spa';
  navLinks = [
    {
      path: 'home',
      label: 'Home'
    },
    {
      path: 'fossils3d',
      label: 'Fossils 3D'
    },
    {
      path: 'about',
      label: 'About'
    }
  ];
  public useMobileSideNav = this.breakpointObserver.observe([Breakpoints.Handset]).pipe(
    map(it => it.matches)
  );
  public toolbarHeight = this.useMobileSideNav.pipe(
    map(it => it ? 56 : 64)
  );
  private deepestRouteData = this.router.events.pipe(
    filter(it => {
      return it instanceof ActivationStart;
    }),
    map(it => (it as ActivationStart).snapshot.data)
  );
  public navigationMode = (() => {
    const subject = new BehaviorSubject<string | undefined>(undefined);
    this.deepestRouteData.pipe(
      map(it => it.navigationMode)
    ).subscribe(subject);
    return subject;
  })();
  @ViewChild('matSidenav', {static: false})
  private matSidenav: MatSidenav;

  constructor(private breakpointObserver: BreakpointObserver, private router: Router, public location: Location) {
    this.navigationMode.pipe(
      filter(it => it !== undefined)
    ).subscribe(() => {
      // noinspection JSIgnoredPromiseFromCall
      this.matSidenav.close();
    });
  }

  onPanRight($event: HammerInput) {
    if ($event.pointerType !== 'mouse' && this.navigationMode.value === undefined) {
      return this.matSidenav.open();
    }
  }

  onPanLeft($event: HammerInput) {
    if ($event.pointerType !== 'mouse' && this.navigationMode.value === undefined) {
      return this.matSidenav.close();
    }
  }
}
