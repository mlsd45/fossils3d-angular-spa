import {Attribute, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';
import startCase from 'lodash-es/startCase';

@JsonApiModelConfig({
  type: 'rank'
})
export class Rankv2 extends JsonApiModel {
  @Attribute()
  id: string;

  @Attribute()
  ordering: number;

  get name() {
    return startCase(this.id);
  }
}
