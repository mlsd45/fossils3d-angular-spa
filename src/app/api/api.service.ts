import {Injectable} from '@angular/core';
import {DatastoreConfig, JsonApiDatastore, JsonApiDatastoreConfig} from 'angular2-jsonapi';
import {HttpClient} from '@angular/common/http';
import {Fossilv2} from './fossils/fossilv2';
import {Rankv2} from './ranks/rankv2';
import {RockUnitv2} from './rock-units/rock-unitv2';
import {Taxonv2} from './taxa/taxonv2';
import {TimeDivisionv2} from './time-divisions/time-divisionv2';
import {TimeScalev2} from './time-scales/time-scalev2';

const config: DatastoreConfig = {
  baseUrl: './api/v2',
  models: {
    fossil: Fossilv2,
    rank: Rankv2,
    rockUnit: RockUnitv2,
    taxon: Taxonv2,
    timeDivision: TimeDivisionv2,
    timeScale: TimeScalev2
  }
};

@Injectable({
  providedIn: 'root'
})
@JsonApiDatastoreConfig(config)
export class ApiService extends JsonApiDatastore {

  constructor(http: HttpClient) {
    super(http);
  }
}
