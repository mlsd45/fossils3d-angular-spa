import {Attribute, BelongsTo, HasMany, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';
import {Taxonv2} from '../taxa/taxonv2';
import {RockUnitv2} from '../rock-units/rock-unitv2';

@JsonApiModelConfig({
  type: 'fossil'
})
export class Fossilv2 extends JsonApiModel {
  @Attribute()
  name: string;

  @Attribute()
  modelId: string;

  @Attribute()
  collectionNumber: number;

  @HasMany()
  taxa: Taxonv2[];

  @BelongsTo()
  rockUnit: RockUnitv2;
}
