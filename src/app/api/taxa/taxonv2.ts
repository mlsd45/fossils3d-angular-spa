import {BelongsTo, HasMany, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';
import {Rankv2} from '../ranks/rankv2';
import {Fossilv2} from '../fossils/fossilv2';
import startCase from 'lodash-es/startCase';

@JsonApiModelConfig({
  type: 'taxon'
})
export class Taxonv2 extends JsonApiModel {
  @HasMany()
  children: Taxonv2[];

  @HasMany()
  members: Fossilv2[];

  @BelongsTo()
  rank: Rankv2;

  @BelongsTo()
  parent: Taxonv2;

  get name() {
    return startCase(this.id);
  }
}

