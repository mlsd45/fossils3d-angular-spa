import {Attribute, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'rockUnit'
})
export class RockUnitv2 extends JsonApiModel {
  @Attribute()
  id: string;
  @Attribute()
  name: string;
}
