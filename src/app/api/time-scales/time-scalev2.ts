import {Attribute, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'timeScale'
})
export class TimeScalev2 extends JsonApiModel {
  @Attribute()
  id: string;
  @Attribute()
  name: string;
}
