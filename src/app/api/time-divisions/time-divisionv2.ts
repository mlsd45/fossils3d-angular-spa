import {Attribute, JsonApiModel, JsonApiModelConfig} from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'timeDivision'
})
export class TimeDivisionv2 extends JsonApiModel {
  @Attribute()
  id: string;
  @Attribute()
  name: string;
}

