import {inject, TestBed} from '@angular/core/testing';

import {ApiService} from './api.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Fossilv2} from './fossils/fossilv2';

describe('ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
  }));

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  it('should request at the correct endpoint',
    inject([HttpTestingController, ApiService], (http: HttpTestingController, api: ApiService) => {
      api.findAll(Fossilv2, {include: 'taxa.members'}).subscribe((response) => {
        expect(response).toBeTruthy();
      });

      const request = http.expectOne('./api/v2/fossil?include=taxa.members');
      request.flush(sampleData);
    }));

  it('should parse one fossil',
    inject([HttpTestingController, ApiService], (http: HttpTestingController, api: ApiService) => {
      api.findAll(Fossilv2, {include: 'taxa.members'}).subscribe((response) => {
        expect(response.getModels().length).toBe(1);
      });

      const request = http.expectOne('./api/v2/fossil?include=taxa.members');
      request.flush(sampleData);
    }));

  it('should parse 7 taxa',
    inject([HttpTestingController, ApiService], (http: HttpTestingController, api: ApiService) => {
      api.findAll(Fossilv2, {include: 'taxa.members'}).subscribe((response) => {
        expect(response.getModels()[0].taxa.length).toBe(7);
      });

      const request = http.expectOne('./api/v2/fossil?include=taxa.members');
      request.flush(sampleData);
    }));

  it('should parse 7 taxa that have one member each',
    inject([HttpTestingController, ApiService], (http: HttpTestingController, api: ApiService) => {
      api.findAll(Fossilv2, {include: 'taxa.members'}).subscribe((response) => {
        response.getModels()[0].taxa.forEach((taxon) => {
          expect(taxon.members.length).toBe(1, `Failing taxon: ${taxon.id}`);
        });
      });

      const request = http.expectOne('./api/v2/fossil?include=taxa.members');
      request.flush(sampleData);
    }));
});

export const sampleData = {
  data: [
    {
      id: 'lab10_q5c',
      type: 'fossil',
      attributes: {
        modelId: '921d58b32ea14f4fbbfe643935a861fa',
        collectionNumber: null,
        name: 'Lab 10 Question 5C Echinarachnius'
      },
      relationships: {
        taxa: {
          data: [
            {
              id: 'echinarachnius',
              type: 'taxon'
            },
            {
              id: 'echinarachniidae',
              type: 'taxon'
            },
            {
              id: 'clypeasteroida',
              type: 'taxon'
            },
            {
              id: 'echinoidea',
              type: 'taxon'
            },
            {
              id: 'echinodermata',
              type: 'taxon'
            },
            {
              id: 'animalia',
              type: 'taxon'
            },
            {
              id: 'eukarya',
              type: 'taxon'
            }
          ],
          links: {
            self: 'https://dev.skaggsm.com/api/v2/fossil/lab10_q5c/relationships/taxa',
            related: 'https://dev.skaggsm.com/api/v2/fossil/lab10_q5c/taxa'
          }
        },
        rockUnit: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/fossil/lab10_q5c/relationships/rockUnit',
            related: 'https://dev.skaggsm.com/api/v2/fossil/lab10_q5c/rockUnit'
          }
        }
      },
      links: {
        self: 'https://dev.skaggsm.com/api/v2/fossil/lab10_q5c'
      }
    }
  ],
  included: [
    {
      id: 'animalia',
      type: 'taxon',
      relationships: {
        parent: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/animalia/relationships/parent',
            related: 'https://dev.skaggsm.com/api/v2/taxon/animalia/parent'
          }
        },
        children: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/animalia/relationships/children',
            related: 'https://dev.skaggsm.com/api/v2/taxon/animalia/children'
          }
        },
        members: {
          data: [
            {
              id: 'lab10_q5c',
              type: 'fossil'
            }
          ],
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/animalia/relationships/members',
            related: 'https://dev.skaggsm.com/api/v2/taxon/animalia/members'
          }
        },
        rank: {
          data: {
            id: 'kingdom',
            type: 'rank'
          },
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/animalia/relationships/rank',
            related: 'https://dev.skaggsm.com/api/v2/taxon/animalia/rank'
          }
        }
      },
      links: {
        self: 'https://dev.skaggsm.com/api/v2/taxon/animalia'
      }
    },
    {
      id: 'clypeasteroida',
      type: 'taxon',
      relationships: {
        parent: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida/relationships/parent',
            related: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida/parent'
          }
        },
        children: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida/relationships/children',
            related: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida/children'
          }
        },
        members: {
          data: [
            {
              id: 'lab10_q5c',
              type: 'fossil'
            }
          ],
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida/relationships/members',
            related: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida/members'
          }
        },
        rank: {
          data: {
            id: 'order',
            type: 'rank'
          },
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida/relationships/rank',
            related: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida/rank'
          }
        }
      },
      links: {
        self: 'https://dev.skaggsm.com/api/v2/taxon/clypeasteroida'
      }
    },
    {
      id: 'echinarachniidae',
      type: 'taxon',
      relationships: {
        parent: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae/relationships/parent',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae/parent'
          }
        },
        children: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae/relationships/children',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae/children'
          }
        },
        members: {
          data: [
            {
              id: 'lab10_q5c',
              type: 'fossil'
            }
          ],
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae/relationships/members',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae/members'
          }
        },
        rank: {
          data: {
            id: 'family',
            type: 'rank'
          },
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae/relationships/rank',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae/rank'
          }
        }
      },
      links: {
        self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachniidae'
      }
    },
    {
      id: 'echinarachnius',
      type: 'taxon',
      relationships: {
        parent: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius/relationships/parent',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius/parent'
          }
        },
        children: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius/relationships/children',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius/children'
          }
        },
        members: {
          data: [
            {
              id: 'lab10_q5c',
              type: 'fossil'
            }
          ],
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius/relationships/members',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius/members'
          }
        },
        rank: {
          data: {
            id: 'genus',
            type: 'rank'
          },
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius/relationships/rank',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius/rank'
          }
        }
      },
      links: {
        self: 'https://dev.skaggsm.com/api/v2/taxon/echinarachnius'
      }
    },
    {
      id: 'echinodermata',
      type: 'taxon',
      relationships: {
        parent: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata/relationships/parent',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata/parent'
          }
        },
        children: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata/relationships/children',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata/children'
          }
        },
        members: {
          data: [
            {
              id: 'lab10_q5c',
              type: 'fossil'
            }
          ],
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata/relationships/members',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata/members'
          }
        },
        rank: {
          data: {
            id: 'phylum',
            type: 'rank'
          },
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata/relationships/rank',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata/rank'
          }
        }
      },
      links: {
        self: 'https://dev.skaggsm.com/api/v2/taxon/echinodermata'
      }
    },
    {
      id: 'echinoidea',
      type: 'taxon',
      relationships: {
        parent: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea/relationships/parent',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea/parent'
          }
        },
        children: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea/relationships/children',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea/children'
          }
        },
        members: {
          data: [
            {
              id: 'lab10_q5c',
              type: 'fossil'
            }
          ],
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea/relationships/members',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea/members'
          }
        },
        rank: {
          data: {
            id: 'class',
            type: 'rank'
          },
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea/relationships/rank',
            related: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea/rank'
          }
        }
      },
      links: {
        self: 'https://dev.skaggsm.com/api/v2/taxon/echinoidea'
      }
    },
    {
      id: 'eukarya',
      type: 'taxon',
      relationships: {
        parent: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/eukarya/relationships/parent',
            related: 'https://dev.skaggsm.com/api/v2/taxon/eukarya/parent'
          }
        },
        children: {
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/eukarya/relationships/children',
            related: 'https://dev.skaggsm.com/api/v2/taxon/eukarya/children'
          }
        },
        members: {
          data: [
            {
              id: 'lab10_q5c',
              type: 'fossil'
            }
          ],
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/eukarya/relationships/members',
            related: 'https://dev.skaggsm.com/api/v2/taxon/eukarya/members'
          }
        },
        rank: {
          data: {
            id: 'domain',
            type: 'rank'
          },
          links: {
            self: 'https://dev.skaggsm.com/api/v2/taxon/eukarya/relationships/rank',
            related: 'https://dev.skaggsm.com/api/v2/taxon/eukarya/rank'
          }
        }
      },
      links: {
        self: 'https://dev.skaggsm.com/api/v2/taxon/eukarya'
      }
    }
  ],
  links: {
    first: 'https://dev.skaggsm.com/api/v2/fossil?page[limit]=20&include=taxa.members',
    last: 'https://dev.skaggsm.com/api/v2/fossil?page[limit]=20&include=taxa.members&page[offset]=40',
    next: 'https://dev.skaggsm.com/api/v2/fossil?page[limit]=20&include=taxa.members&page[offset]=20'
  },
  meta: {
    totalResourceCount: 1
  }
};

