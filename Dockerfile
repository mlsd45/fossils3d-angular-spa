FROM nginx:stable-alpine
COPY default.nginx /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
COPY dist/fossils3d-angular-spa /etc/nginx/html
