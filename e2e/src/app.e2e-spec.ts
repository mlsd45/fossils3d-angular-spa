import {browser, logging} from 'protractor';

describe('workspace-project App', () => {

  beforeEach(() => {
    browser.get(browser.baseUrl);
  });

  it('should have a title', () => {
    expect(browser.getTitle()).toContain('Fossils');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
